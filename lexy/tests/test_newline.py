import io

from lexy import BaseLexer, Newline, Identifier, Integer, String, newline
from lexy.tests.utils import expect_tokens

class Lexer(BaseLexer):
    yield_newlines = True
    line_comment_delimiter = ';'

@expect_tokens('\n', [newline], Lexer)
def test_single_newline(tokens):
    pass

@expect_tokens('\n\n', [newline, newline], Lexer)
def test_two_newlines(tokens):
    pass

@expect_tokens(
    '"Hello"\n" world!"', [String('Hello'), newline, String(' world!')],
    Lexer)
def test_two_strings(tokens):
    pass

@expect_tokens(
    '"Hello world!"\n255\nTest', [
        String('Hello world!'), newline,
        Integer(255), newline,
        Identifier('Test')
    ], Lexer)
def test_many_kins(tokens):
    pass

@expect_tokens(
    'ident1 ; comment\n ident2',
    [Identifier('ident1'), newline, Identifier('ident2')],
    Lexer
)
def test_comment_newline(tokens):
    pass
