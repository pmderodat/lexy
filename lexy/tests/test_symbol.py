import io

from lexy import BaseLexer, Symbol, Keyword, Integer
from lexy.tests.utils import expect_tokens

class Lexer(BaseLexer):
    symbols = set(('(', ')', '.', '=', '+', '-', '+=', '=+', '...+'))
    keywords = set('print set'.split())

@expect_tokens('(', [Symbol('(')], Lexer)
def test_single_simple_symbol(tokens):
    pass

@expect_tokens('+=', [Symbol('+=')], Lexer)
def test_single_complex_symbol(tokens):
    pass

@expect_tokens('()', [Symbol('('), Symbol(')')], Lexer)
def test_two_simple_symbols(tokens):
    pass

@expect_tokens('+=+', [Symbol('+='), Symbol('+')], Lexer)
def test_two_complex_symbols(tokens):
    pass

@expect_tokens('...+', [Symbol('...+')], Lexer)
def test_long_symbol(tokens):
    pass

@expect_tokens(
    '...-', [Symbol('.'), Symbol('.'), Symbol('.'), Symbol('-')], Lexer)
def test_small_symbols_look_like_a_long_one(tokens):
    pass

@expect_tokens('...+-', [Symbol('...+'), Symbol('-')], Lexer)
def test_another_one_on_long_symbols(tokens):
    pass

@expect_tokens('print(', [Keyword('print'), Symbol('(')], Lexer)
def test_word_with_symbol(tokens):
    pass

@expect_tokens('(set', [Symbol('('), Keyword('set')], Lexer)
def test_symbol_with_word(tokens):
    pass

@expect_tokens('01(', [Integer(1), Symbol('(')], Lexer)
def test_symbol_with_number(tokens):
    pass

@expect_tokens('(12', [Symbol('('), Integer(12)], Lexer)
def test_keyword_with_number(tokens):
    pass
