import io

from lexy import BaseLexer, Symbol, Identifier
from lexy.tests.utils import expect_tokens

class Lexer1(BaseLexer):
    line_comment_delimiter = '--='

class Lexer2(BaseLexer):
    symbols = set('--')
    line_comment_delimiter = '--='

class Lexer3(BaseLexer):
    line_comment_delimiter = ';'

@expect_tokens(
    'hello--=comment\nworld', [Identifier('hello'), Identifier('world')],
    Lexer1)
def test_line_comment(tokens):
    pass

@expect_tokens(
    'hello--=comment\nworld', [Identifier('hello'), Identifier('world')],
    Lexer2)
def test_line_comment_symbol_ambiguous(tokens):
    pass

@expect_tokens(
    'hello ; comment\nworld', [Identifier('hello'), Identifier('world')],
    Lexer3)
def test_line_commend_one_char(tokens):
    pass
