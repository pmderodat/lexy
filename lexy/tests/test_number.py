import io

from lexy import BaseLexer, Integer, Decimal, Identifier, Symbol
from lexy.lexer import InvalidToken

from lexy.tests.utils import expect_tokens, expect_error

@expect_tokens('12', [Integer(12)])
def test_single(tokens):
    pass

@expect_tokens('0    42', [Integer(0), Integer(42)])
def test_two_integers(tokens):
    pass

@expect_error('42u')
def test_invalid_number(error):
    pass

@expect_tokens('0b0100', [Integer(0b100)])
def test_binary(tokens):
    pass

@expect_tokens('0o12347', [Integer(0o12347)])
def test_octal(tokens):
    pass

@expect_tokens('0xdeadbeef', [Integer(0xdeadbeef)])
def test_hexadecimal(tokens):
    pass

@expect_error('0b12')
def test_bad_binary(error):
    pass

@expect_error('0o18')
def test_bad_octal(error):
    pass

@expect_error('0xfg')
def test_bad_hexadecimal(error):
    pass

@expect_tokens('-1', [Integer(-1)])
def test_negative(tokens):
    pass

@expect_tokens('0.2', [Decimal(0.2)])
def test_simple_decimal(tokens):
    pass

@expect_tokens('-0.3', [Decimal(-0.3)])
def test_negative_decimal(tokens):
    pass

@expect_error('1.0end')
def test_decimal_identifier(error):
    pass

@expect_tokens('1.', [Decimal(1.0)])
def test_decimal_without_suffix(tokens):
    pass

@expect_error('1.end')
def test_decimal_without_suffix_identifier(error):
    pass

@expect_tokens('0.0 0.1', [Decimal(0.0), Decimal(0.1)])
def test_two_decimals(tokens):
    pass

@expect_tokens('print .0 end', [
    Identifier('print'), Decimal(0.0), Identifier('end')
])
def test_decimal_without_prefix(tokens):
    pass

@expect_tokens('print -.1 end', [
    Identifier('print'), Decimal(-.1), Identifier('end')
])
def test_negative_decimal_without_prefix(tokens):
    pass

class StrangeLexer(BaseLexer):
    symbols = set(('.', '-'))
@expect_tokens('-..', [Symbol('-'), Symbol('.'), Symbol('.')], StrangeLexer)
def test_symbols_that_look_like_negative_decimal(tokens):
    pass
