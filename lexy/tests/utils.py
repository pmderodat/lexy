from functools import wraps
import io

from lexy import BaseLexer
from lexy.lexer import InvalidToken

def expect_tokens(input_str, tokens, lexer=BaseLexer):

    def decorator(func):
        @wraps(func)
        def wrapper():
            s = io.StringIO(input_str)
            yield_tokens = list(lexer(s))
            try:
                assert (
                    tokens is None or
                    yield_tokens == tokens
                )
            except AssertionError:
                print(
                    'Expected tokens were:\n'
                    '    {}\n'
                    'But yield tokens are:\n'
                    '    {}'.format(tokens, yield_tokens)
                )
                raise
            func(yield_tokens)

        return wrapper
    return decorator


def expect_error(input_str, lexer=BaseLexer):

    def decorator(func):
        @wraps(func)
        def wrapper():
            s = io.StringIO(input_str)
            try:
                tokens = list(lexer(s))
            except InvalidToken as e:
                error = e
            else:
                assert False, (
                    'I was expecting an error. '
                    'Instead, I got tokens:\n'
                    '    {}'.format(tokens)
                )
            func(error)

        return wrapper
    return decorator
