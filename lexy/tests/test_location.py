import io

from lexy import BaseLexer, Keyword, Identifier, String
from lexy.lexer import InvalidToken
from lexy.token import Position, Location
from lexy.tests.utils import expect_tokens

def get_locations(tokens):
    return [
        tuple(token.location.start) + tuple(token.location.end)
        for token in tokens
    ]

class Lexer(BaseLexer):
    keywords = set(('print', ))
    line_comment_delimiter = '#'

@expect_tokens('print "hello world!"   ', None, Lexer)
def test_simple(tokens):
    assert get_locations(tokens) == [(1, 1, 1, 5), (1, 7, 1, 20)]

@expect_tokens('print "hello world!" # Basic output\n   1', None, Lexer)
def test_two_lines(tokens):
    assert get_locations(tokens) == [
        (1, 1, 1, 5), (1, 7, 1, 20),
        (2, 4, 2, 4),
    ]
