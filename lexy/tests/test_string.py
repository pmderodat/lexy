import io

from lexy import BaseLexer, Keyword, Identifier, String
from lexy.lexer import InvalidToken
from lexy.tests.utils import expect_tokens, expect_error

class Lexer(BaseLexer):
    keywords = set(('print', ))
    line_comment_delimiter = '#'

@expect_tokens(
    'print "hello world!"', [Keyword('print'), String('hello world!')], Lexer)
def test_simple(tokens):
    pass

@expect_tokens('"hello # world!"', [String('hello # world!')])
def test_no_comment(tokens):
    pass

@expect_tokens(r'"\e[31m"', [String('\x1b[31m')])
def test_escape(tokens):
    pass

@expect_tokens(r'"\x01"', [String('\x01')])
def test_escape_hex(tokens):
    pass

@expect_error('"hello', Lexer)
def test_unmatched_quote(error):
    pass

@expect_error('"', Lexer)
def test_unmatched_quote_empty(error):
    pass

@expect_error(r'"\_"', Lexer)
def test_bad_escape(error):
    pass

@expect_error(r'"\x0"', Lexer)
def test_bad_escape(error):
    pass

@expect_error(r'"\x0z"', Lexer)
def test_bad_escape_bad_hex(error):
    pass

@expect_tokens('"hello" " world!"', [String('hello world!')])
def test_multiple_string(tokens):
    pass

@expect_tokens('"hello"" world!"', [String('hello world!')])
def test_multiple_string_no_space(tokens):
    pass
