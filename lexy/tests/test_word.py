import io

from lexy import BaseLexer, Keyword, Identifier
from lexy.tests.utils import expect_tokens

class Lexer(BaseLexer):
    keywords = set('print set'.split())

@expect_tokens('print', [Keyword('print')], Lexer)
def test_single_keyword(tokens):
    pass

@expect_tokens('my_1st_var', [Identifier('my_1st_var')], Lexer)
def test_single_identifier(tokens):
    pass

@expect_tokens('print hello0 _world set', [
        Keyword('print'),
        Identifier('hello0'),
        Identifier('_world'),
        Keyword('set'),
    ], Lexer)
def test_multiple_words(tokens):
    pass
