from lexy.token import Token, Identifier, Integer



def test_hashable_equal():
    t1 = Identifier('id')
    t2 = Identifier('id')
    assert hash(t1) == hash(t2)

def test_hashable_different():
    t1 = Identifier('id1')
    t2 = Identifier('id2')
    assert hash(t1) != hash(t2)

def test_equal_wrong_type():
    assert Identifier('id') != None
