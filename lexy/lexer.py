# -*- coding: utf-8 -*-

from lexy.token import *



class LexingError(Exception):
    pass

class InvalidToken(LexingError):
    def __init__(self, message, location):
        super(InvalidToken, self).__init__(message, location)

    def __str__(self):
        return self.args[0]

class BacktrackStream:
    '''
    Wrap a readonly stream in order to let user put data back in it.
    '''

    def __init__(self, embedded_stream):
        self.line = 1
        self.column = 1
        self.last_line_length = -1
        self.embedded_stream = embedded_stream
        self.buffer = ''

    def get_position(self, before=False):
        '''
        Return the position of the cursor.
        '''
        return Position(self.line, self.column - 1 if before else self.column)

    def _update_location_forwards(self, string):
        '''
        Update internal location taking the given string into account to go
        forwards.
        '''
        for c in string:
            if c == '\n':
                self.last_line_length = self.column - 1
                self.line += 1
                self.column = 1
            else:
                self.column += 1

    def _update_location_backwards(self, string):
        '''
        Update internal location taking the given string into account to go
        backwards.
        '''
        for c in reversed(string):
            if c == '\n':
                if self.last_line_length == -1:
                    # Putting back more than one line should not happen.
                    raise LexingError('More than one line was being put back.')
                self.line -= 1
                self.column = self.last_line_length
                self.last_line_length = -1
            else:
                self.column -= 1

    def read(self, size):
        '''
        Read 'size' bytes from the stream.
        '''
        result = self.buffer[:size]
        self.buffer = self.buffer[size:]
        if len(result) < size:
            result += self.embedded_stream.read(size - len(result))
        self._update_location_forwards(result)
        return result

    def readline(self, stop_before_newline=False):
        '''
        Read bytes in the stream until the next newline character.

        The trailing newline character is kept in the stream (and thus not
        returned) if stop_before_newline.
        '''
        result = ''
        for i, byte in enumerate(self.buffer):
            if byte == '\n':
                limit = i if stop_before_newline else i + 1
                result = self.buffer[limit:]
                self.buffer = self.buffer[:limit]
                return result
        result = self.buffer + self.embedded_stream.readline()
        if stop_before_newline and result[-1:] == '\n':
            self.buffer = '\n'
            result = result[:-1]
        else:
            self.buffer = ''
        self._update_location_forwards(result)
        return result

    def put_back(self, bytes):
        '''
        Put back 'bytes' to the stream. These will be returned the next time
        'read' is called.
        '''
        self.buffer = bytes + self.buffer
        self._update_location_backwards(bytes)

class BaseLexer(object):

    # List of available symbols
    symbols = set()
    # List of available keywords
    keywords = set()

    # Delimiter for single-line comments. None means: single-line comment are
    # not available.
    line_comment_delimiter = None

    # If true, return tokens for newline characters. A side effect is that
    # the lexer doesn’t merge consecutive string tokens: it is up to the parser
    # to do that.
    yield_newlines = False

    DECIMAL = 10
    BINARY = 2
    OCTAL = 8
    HEXADECIMAL = 16
    BASE = {
        'b': BINARY,
        'o': OCTAL,
        'x': HEXADECIMAL,
    }

    ESCAPE_CHARS = {
        '"': '"',
        '\\': '\\',
        '0': '\x00',
        'a': '\x07',
        'b': '\x08',
        't': '\x09',
        'n': '\x0A',
        'v': '\x0B',
        'f': '\x0C',
        'r': '\x0D',
        'e': '\x1b',
    }

    def __init__(self, stream):
        self.stream = BacktrackStream(stream)

        # The extended symbol set also contains comment delimiters.
        self.extended_symbol_set = set(self.symbols)
        if self.line_comment_delimiter:
            self.extended_symbol_set.add(self.line_comment_delimiter)

        self.symbol_max_size = (
            max(len(symbol) for symbol in self.extended_symbol_set)
            if self.extended_symbol_set else
            0
        )

        self.buffer = ''

    def __iter__(self):
        return self

    def __next__(self):
        while True:
            self.token_start = self.stream.get_position()
            self.buffer = self.stream.read(1)
            if not self.buffer:
                raise StopIteration()
            result = ''
            if self.buffer == '-':
                result = self.dispatch_dash()
            elif self.buffer == '.':
                result = self.dispatch_dot()
            elif self.buffer == '"':
                result = self.read_string()
            elif self.buffer.isdigit():
                result = self.read_number()
            elif self.can_start_identifier(self.buffer):
                result = self.read_word()
            elif self.yield_newlines and self.buffer == '\n':
                result = self.flush_and_return(Newline())
            elif self.buffer.isspace():
                continue
            else:
                result = self.read_symbol()

            if result:
                return result

    def next(self):
        return self.__next__()

    def get_current_location(self, last_token_end=None):
        return Location(
            self.token_start,
            last_token_end or self.stream.get_position(before=True)
        )

    def flush_and_return(self, token, back_bytes='', last_token_end=None):
        '''
        Flush the buffer, put back some bytes in the stream and return the
        given data.
        '''
        self.buffer = ''
        self.stream.put_back(back_bytes)
        token.location = self.get_current_location(last_token_end)
        return token

    def dispatch_dash(self):
        next_char = self.stream.read(1)
        if next_char.isdigit():
            self.buffer = next_char
            return self.read_number(negative=True)
        elif next_char == '.':
            next_next_char = self.stream.read(1)
            if next_next_char.isdigit():
                self.buffer = next_char + next_next_char
                return self.read_number(negative=True, is_decimal=True)
            else:
                self.stream.put_back(next_char + next_next_char)
        else:
            self.stream.put_back(next_char)
        return self.read_symbol()

    def dispatch_dot(self):
        next_char = self.stream.read(1)
        if next_char.isdigit():
            self.buffer = '.' + next_char
            return self.read_number(is_decimal=True)
        else:
            self.stream.put_back(next_char)
            return self.read_symbol()

    def read_string(self):
        self.buffer = ''
        mode = 'default'
        token_end = None
        while True:
            if mode == 'hex':
                char_number = self.stream.read(2)
                if len(char_number) != 2:
                    raise InvalidToken(
                        'Invalid escape sequence',
                        self.get_current_location()
                    )
                try:
                    char_number = int(char_number, 16)
                except ValueError:
                    raise InvalidToken(
                        'Invalid escape sequence: \\x' + char_number,
                        self.get_current_location()
                    )
                mode = 'default'
                self.buffer += chr(char_number)
                continue

            next_char = self.stream.read(1)
            if mode == 'outside':
                if next_char == '"':
                    mode = 'default'
                elif not next_char.isspace():
                    return self.flush_and_return(
                        String(self.buffer), next_char,
                        last_token_end=token_end
                    )
            elif not next_char:
                raise InvalidToken(
                    'Unmatched string terminator',
                    self.get_current_location()
                )
            elif mode == 'escape' and next_char == 'x':
                mode = 'hex'
            elif mode == 'escape':
                try:
                    self.buffer += self.ESCAPE_CHARS[next_char]
                except KeyError:
                    raise InvalidToken(
                        'Invalid escape sequence: \\' + next_char,
                        get_current_location()
                    )
                mode = 'default'
            elif next_char == '"':
                token_end = self.stream.get_position(before=True)
                if self.yield_newlines:
                    return self.flush_and_return(String(self.buffer))
                mode = 'outside'
            elif next_char == '\\':
                mode = 'escape'
            else:
                self.buffer += next_char

    def read_number(self, negative=False, is_decimal=False):

        def get_whole_buffer(next_char=None):
            return '{}{}{}'.format(
                '-' if negative else '',
                self.buffer,
                next_char or ''
            )

        def return_number(next_digit=''):
            if negative:
                self.buffer = '-' + self.buffer
            try:
                result = (
                    Decimal(self.buffer)
                    if is_decimal else
                    Integer(self.buffer, base)
                )
            except ValueError:
                raise InvalidToken(
                    get_whole_buffer(next_digit),
                    self.get_current_location()
                )
            return self.flush_and_return(result, next_digit)

        base = self.DECIMAL
        while True:
            next_digit = self.stream.read(1)
            if not next_digit or next_digit.isspace():
                return return_number(next_digit)
            if self.buffer == '0' and next_digit.lower() in 'box':
                base = self.BASE[next_digit.lower()]
            elif base == self.HEXADECIMAL and next_digit.lower() in 'abcdef':
                self.buffer += next_digit
            elif self.can_start_identifier(next_digit):
                raise InvalidToken(
                    get_whole_buffer(next_digit),
                    self.get_current_location()
                )
            elif next_digit.isdigit():
                self.buffer += next_digit
            elif next_digit == '.':
                if base != self.DECIMAL or is_decimal:
                    raise InvalidToken(
                        get_whole_buffer(next_digit),
                        self.get_current_location()
                    )
                self.buffer += '.'
                is_decimal = True
            else:
                self.stream.put_back(next_digit)
                return return_number()

    def read_word(self):
        while True:
            next_char = self.stream.read(1)
            if not next_char:
                return self.flush_and_return(self.make_word_token())
            elif self.can_start_identifier(next_char) or next_char.isdigit():
                self.buffer += next_char
            else:
                return self.flush_and_return(self.make_word_token(), next_char)

    def make_word_token(self):
        if self.buffer in self.keywords:
            return Keyword(self.buffer)
        else:
            return Identifier(self.buffer)

    def read_symbol(self):
        last_valid_symbol = 1 if self.buffer in self.extended_symbol_set else 0
        while len(self.buffer) <= self.symbol_max_size:
            next_char = self.stream.read(1)
            if not next_char:
                break

            self.buffer += next_char
            if self.buffer in self.extended_symbol_set:
                last_valid_symbol = len(self.buffer)
            elif (
                next_char.isspace() or next_char.isdigit() or
                self.can_start_identifier(next_char)
            ):
                break

        if last_valid_symbol > 0:
            self.stream.put_back(self.buffer[last_valid_symbol:])
            symbol = self.buffer[:last_valid_symbol]
            if symbol == self.line_comment_delimiter:
                self.stream.readline(stop_before_newline=True)
                return
            else:
                return self.flush_and_return(Symbol(symbol))
        raise InvalidToken(self.buffer, self.get_current_location())

    def can_start_identifier(self, byte):
        return byte.isalpha() or byte == '_'
