# Lexy version
VERSION = 'dev'

from lexy.lexer import BaseLexer, LexingError
from lexy.token import (
    Newline, Symbol, Keyword, Identifier, Integer, Decimal, String,
    newline, symbol, keyword, identifier, integer, decimal, string
)
