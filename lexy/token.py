from collections import namedtuple
from itertools import count


Position = namedtuple('Position', 'line column')
Location = namedtuple('Location', 'start end')


class Token(object):
    # Each token type has an identifier. These identifiers are generated using
    # a counter.
    type_counter = count()

    NEWLINE = next(type_counter)
    SYMBOL = next(type_counter)
    KEYWORD = next(type_counter)
    IDENTIFIER = next(type_counter)
    INTEGER = next(type_counter)
    DECIMAL = next(type_counter)
    STRING = next(type_counter)

    PRETTY_TYPES = {
        NEWLINE: 'newline',
        SYMBOL: 'symbol',
        KEYWORD: 'keyword',
        IDENTIFIER: 'identifier',
        INTEGER: 'integer',
        DECIMAL: 'decimal',
        STRING: 'string',
    }

    __slots__ = ['type', 'value', 'location']

    def __init__(self, type, value=None, location=None):
        self.type = type
        self.value = value
        self.location = location

    def __str__(self):
        return '{}{}'.format(
            self.PRETTY_TYPES[self.type],
            ' {}'.format(self.value)
            if self.value is not None else
            ''
        )

    def __repr__(self):
        return '{}{}'.format(
            self.PRETTY_TYPES[self.type],
            '({})'.format(repr(self.value))
            if self.value is not None else
            ''
        )

    def __hash__(self):
        return hash((self.type, self.value))

    is_newline = lambda self: self.type == Token.NEWLINE
    is_symbol = lambda self: self.type == Token.SYMBOL
    is_keyword = lambda self: self.type == Token.KEYWORD
    is_identifier = lambda self: self.type == Token.IDENTIFIER
    is_integer = lambda self: self.type == Token.INTEGER
    is_decimal = lambda self: self.type == Token.DECIMAL
    is_string = lambda self: self.type == Token.STRING

    def __eq__(self, token):
        try:
            token_type = token.type
            token_value = token.value
        except AttributeError:
            # If the othen token misses at least one attribute, it *cannot* be
            # equal!
            return False
        return (
            self.type == token_type and (
                self.value is None or
                token_value is None or
                self.value == token_value
            )
        )

def Newline(location=None):
    return Token(Token.NEWLINE, '\n', location)

def Symbol(value, location=None):
    return Token(Token.SYMBOL, value, location)

def Keyword(value, location=None):
    return Token(Token.KEYWORD, value, location)

def Identifier(value, location=None):
    return Token(Token.IDENTIFIER, value, location)

def Integer(value, base=10, location=None):
    return Token(
        Token.INTEGER,
        None
        if value is None else
        (
            value
            if isinstance(value, int) else
            int(value, base)
        ), location
    )

def Decimal(value, location=None):
    return Token(
        Token.DECIMAL,
        None
        if value is None else
        (
            value
            if isinstance(value, float) else
            float(value)
        ), location
    )

def String(value, location=None):
    return Token(Token.STRING, value, location)


newline = Newline()
symbol = Symbol(None)
keyword = Keyword(None)
identifier = Identifier(None)
integer = Integer(None)
decimal = Decimal(None)
string = String(None)
