from setuptools import setup, find_packages

from lexy import VERSION

setup(
    name = 'Lexy',
    version = VERSION,
    packages = find_packages()
)
