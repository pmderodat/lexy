Lexy
====

Lexy is a pure Python3 lexing library. It aims at staying simple, to be able to
create lexers at runtime, and does not target to be as powerful as something
like `flex`.

To know more about it, explore the source or visit [the project
website](http://lexy.kawie.fr/).
